SHELL := /bin/bash

OK      := "	 \033[32;1m ok \033[0m"
INFO    := "   \033[34;1m info \033[0m"
ERROR   := "  \033[41;1m error \033[0m"
WARNING := "\033[33;1m warning \033[0m"

# Task: help | show this help message.
# {
.PHONY: help
help:
	@echo
	@printf '\033[33mUsage:\033[0m'
	@echo '  make <target> [--] [<command>...] [options]'
	@echo
	@printf '\033[33mTargets:\033[0m\n'
	@egrep '^# Task: ([a-z]+) \| (.+)$$' Makefile | awk -F '[:|]' '{print " \033[32m"$$2"\033[0m" "|" $$3}' | column -t -s '|'
	@echo
# }

# Task: update | Updates fivem server to the latest version.
# {
.PHONY: update
update:
	./scripts/update-server.sh
# }

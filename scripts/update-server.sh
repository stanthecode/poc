#!/bin/bash
function pause(){
   read -p "$*"
}

function installPackages() {
   echo -e "\e[95mTesting if packages are missing...\033[0m"
   
   if [ $(dpkg-query -W -f='${Status}' wget 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
      sudo apt-get install wget;
   fi

   if [ $(dpkg-query -W -f='${Status}' unzip 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
      sudo apt-get install unzip;
   fi

   echo -e " "
}

function clearData() {
   SERVER_DIR=$PWD/server

   rm -rf $SERVER_DIR
   rm server.zip 2>/dev/null

   echo -e "\e[95mCleared files and server folder...\033[0m"
   echo -e " "
}

function downloadServer() {
   SERVER_URL='http://runtime.fivem.net/artifacts/fivem/build_server_windows/master/'
   SERVER_FILE=$(curl -L https://runtime.fivem.net/artifacts/fivem/build_server_windows/master/ 2>/dev/null | grep '<a class="panel-block  is-active"' | grep -oP -m 1 'href="./\K[^"]+')

   wget ${SERVER_URL}${SERVER_FILE}

   unzip server.zip -d $PWD/server
   rm server.zip 2>/dev/null
}

clear

# Header
echo -e "\e[95m"
echo -e "-------------------------------------------------"
echo -e "----- FiveM Server Updater by Stan The Code -----"
echo -e "-------------------------------------------------"

echo -e " "
read -p "Do you want to update server? (Yes/No): " confirm
echo -e "\033[0m"

if [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]]; then
   clearData
   installPackages
   downloadServer
   echo -e "\e[95m" 
   pause 'Server is updated to latest version. Press any key to continue...'
   echo "\033[0m"
   exit 
else
   echo -e "\e[95m"
   pause 'Press any key to exit...'
   echo -e "\033[0m"
   exit
fi

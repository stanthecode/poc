:: Setting paths
SET CURRENT=%cd%
SET SERVER=%CURRENT%\server
SET SERVER_DATA=%CURRENT%\server-data

@echo off
echo Starting fivem server...

:main
cd %SERVER_DATA%
rmdir /q /s cache

%SERVER%/FXServer.exe +exec server.cfg

pause
echo Restarting fivem server...

goto main
